/** @type {import('tailwindcss').Config} */
export default {
  content: ["./views/**/*.hbs", "./routes/**/*.js", "utils.js"],
  theme: {
    extend: {
      keyframes: {
        "widjet-appear": {
          from: {
            bottom: "-100px",
          },
          to: {
            bottom: "20px",
          },
        },
        "widjet-disappear": {
          from: {
            bottom: "20px",
          },
          to: {
            bottom: "-100px",
          },
        },
      },
      animation: {
        "widjet-actions":
          "widjet-appear 1.5s ease-in-out, widjet-disappear 1.5s ease-in-out 7.5s",
      },
    },
  },
  plugins: [require("tailwindcss-animate")],
};
