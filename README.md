# Задание

## Стек

js, prisma, express, Handlebars

## mysql server

для нормальной работы необходимо в файле [.env](./.env) вписать в строку подключения данные, как в локальном сервере

DATABASE_URL="mysql://root:admin@localhost:3306/master"

шпаргалка по этой штуке

mysql://login:password@url:port/master

## Как запустить

```
// Скачивание пакетов
npm i

//Подтягивание миграций
npx prisma migrate dev

//Если нет локально установленного mysql, то можно через докер поднять
docker-compose up

//Запуск
npm run dev

```
