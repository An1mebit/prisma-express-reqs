import { Router } from "express";
import { prisma } from "../index.js";
import { getToastMessage } from "../utils.js";

const router = Router();

router.get("/register", (_, res) => {
  return res.render("registration");
});

router.post("/registration", async (req, res) => {
  try {
    const roles = await prisma.role.findMany();

    await prisma.user.create({
      data: {
        full_name: req.body.full_name,
        login: req.body.login,
        password: req.body.password,
        phone: req.body.phone,
        id_role: roles.filter((item) => item.code === "user").at(0).id,
      },
    });
    return res.send(
      getToastMessage("bg-green-400", "Вы успешно зарегистрировались")
    );
  } catch (error) {
    return res.send(getToastMessage("bg-red-600", "Что-то пошло не так"));
  }
});

export default router;
