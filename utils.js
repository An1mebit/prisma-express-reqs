export async function generateSeedData(db) {
  const roles = await db.role.findMany();
  const masters = await db.master.findMany();
  const admin = await db.user.findFirst({
    where: {
      login: "beauty",
      password: "pass",
    },
  });
  const statuses = await db.status.findMany();

  if (roles.length === 0) {
    const res = await db.role.createMany({
      data: [
        { name: "Зарегистрированный пользователь", code: "user" },
        { name: "Администратор", code: "admin" },
      ],
    });
  }

  if (masters.length === 0) {
    await db.master.createMany({
      data: [{ name: "Иванова Д.Д." }, { name: "Иванова А.Д." }],
    });
  }

  if (statuses.length === 0) {
    await db.status.createMany({
      data: [
        { code: "new", name: "Новое" },
        { code: "canceled", name: "Отменено" },
        { code: "confirmed", name: "Подтверждено" },
      ],
    });
  }

  if (!admin) {
    await db.user.create({
      data: {
        full_name: "admin",
        login: "beauty",
        password: "pass",
        id_role: 2,
        phone: "",
      },
    });
  }
}

export function getToastMessage(color, message) {
  return `
  <div id="message" class="animate-widjet-actions absolute right-0 bottom-[20px] left-0 w-full flex justify-center">
      <div class="${color} rounded-md p-3">
          <p>${message}<p>
      </div>
      <script>
          setTimeout(() => {
              const successMessage = document.getElementById('message');
              successMessage.outerHTML = '<div id="message" class="absolute bottom-0 left-0 right-0"></div>';
          }, [9000])
      </script>
  </div>
`;
}

export function auth(req, res, next) {
  if (req.session && req.session.authenticated) return next();
  else return res.redirect("/login");
}
