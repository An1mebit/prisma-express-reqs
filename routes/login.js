import { Router } from "express";
import { prisma } from "../index.js";
import { getToastMessage } from "../utils.js";

const router = Router();

router.get("/login", (_, res) => {
  return res.render("login");
});

router.post("/logout", async (req, res) => {
  try {
    req.session.destroy();
    res.set("HX-Redirect", "/login");
    return res.status(200).send("Logut successful");
  } catch (error) {
    return res.send(getToastMessage("bg-red-600", "Что-то пошло не так"));
  }
});

router.post("/login", async (req, res) => {
  try {
    const userData = await prisma.user.findFirst({
      where: {
        login: req.body.login,
        password: req.body.password,
      },
    });
    if (userData) {
      req.session.authenticated = true;
      req.session.userId = userData.id;
      res.set("HX-Redirect", "/");
      return res.status(200).send("Login successful");
    } else {
      return res.send(
        getToastMessage("bg-red-600", "Неправильный логин или пароль")
      );
    }
  } catch (error) {
    return res.send(getToastMessage("bg-red-600", "Что-то пошло не так"));
  }
});

export default router;
