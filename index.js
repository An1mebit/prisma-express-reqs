import express from "express";
import exphbs from "express-handlebars";
import session from "express-session";
import path from "path";
import { PrismaClient } from "@prisma/client";
import { generateSeedData } from "./utils.js";

import registrationRoute from "./routes/register.js";
import loginRoute from "./routes/login.js";
import homeRoute from "./routes/home.js";
import requestRoute from "./routes/request.js";

export const prisma = new PrismaClient();
const app = express();
const store = new session.MemoryStore();

generateSeedData(prisma);

const hbs = exphbs.create({
  extname: ".hbs",
  defaultLayout: "main",
  layoutsDir: path.join(path.resolve(process.cwd()), "views", "layouts"),
  partialsDir: path.join(path.resolve(process.cwd()), "views", "partials"),
});

app.engine("hbs", hbs.engine);
app.set("view engine", "hbs");
app.set("views", "views");
app.use(express.static("public"));
app.use(express.urlencoded({ extended: true }));
app.use(express.json({ extended: true }));
app.use(
  session({
    secret: "123123fpdsf",
    name: "_ssid",
    resave: true,
    saveUninitialized: false,
    cookie: {
      secure: false,
      maxAge: 3600000,
      httpOnly: true,
    },
    store,
  })
);

app.use(registrationRoute);
app.use(loginRoute);
app.use(homeRoute);
app.use(requestRoute);

app.listen(3000, () => {
  console.log("started on 3000");
});
