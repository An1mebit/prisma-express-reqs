import { Router } from "express";
import { prisma } from "../index.js";
import { auth, getToastMessage } from "../utils.js";
import { isAfter, isBefore, set } from "date-fns";

const router = Router();

router.get("/create", auth, async (req, res) => {
  try {
    const user = await prisma.user.findFirst({
      include: {
        role: true,
      },
      where: {
        id: req.session.userId,
      },
    });
    const masters = await prisma.master.findMany();
    return res.render("request", {
      ...user,
      isAdmin: user.role.code === "admin",
      options: masters,
    });
  } catch (error) {}
});

router.post("/create", auth, async (req, res) => {
  try {
    const nowDate = new Date(req.body.booking_datetime);
    const startDate = set(nowDate, {
      hours: 8,
      minutes: 0,
      seconds: 0,
      milliseconds: 0,
    });

    const endDate = set(nowDate, {
      hours: 18,
      minutes: 0,
      seconds: 0,
      milliseconds: 0,
    });

    if (isBefore(nowDate, startDate) || isAfter(nowDate, endDate))
      return res.send(
        getToastMessage(
          "bg-red-600",
          "Дата должна быть в диапазоне с 8:00 до 18:00"
        )
      );

    await prisma.request.create({
      data: {
        id_master: +req.body.master_id,
        booking_datetime: new Date(req.body.booking_datetime),
        id_status: 1,
        id_user: req.session.userId,
      },
    });

    res.set("HX-Redirect", "/");
    return res.send(getToastMessage("bg-green-400", "Заявка создана"));
  } catch (error) {
    console.log(error);
    return res.send(getToastMessage("bg-red-600", "Что-то пошло не так"));
  }
});

export default router;
