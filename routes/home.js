import { Router } from "express";
import { prisma } from "../index.js";
import { auth, getToastMessage } from "../utils.js";
import { format } from "date-fns";

const router = Router();

router.get("/", auth, async (req, res) => {
  try {
    const user = await prisma.user.findFirst({
      include: {
        role: true,
      },
      where: {
        id: req.session.userId,
      },
    });
    return res.render("home", { ...user, isAdmin: user.role.code === "admin" });
  } catch (error) {}
});

router.patch("/change-status/:id", auth, async (req, res) => {
  try {
    await prisma.request.update({
      data: {
        id_status: +req.body.id_status,
      },
      where: {
        id: +req.params.id,
      },
    });

    return res.send(getToastMessage("bg-green-400", "Статус успешно изменен"));
  } catch (error) {
    console.log(error);
    return res.send(getToastMessage("bg-red-600", "Что-то пошло не так"));
  }
});

router.get("/requests", auth, async (req, res) => {
  try {
    const user = await prisma.user.findFirst({
      include: {
        role: true,
      },
      where: {
        id: req.session.userId,
      },
    });
    const isAdmin = user.role.code === "admin";

    const filters = isAdmin ? {} : { id_user: req.session.userId };
    const requests = await prisma.request.findMany({
      where: filters,
      include: {
        master: true,
        user: true,
        status: true,
      },
    });

    const statuses = await prisma.status.findMany();

    return res.send(
      requests
        .map(
          (item, index) => `
      <section id="req-card-${index}" class="flex flex-col p-4 rounded-md bg-gray-300 max-w-[450px]">
          ${
            isAdmin
              ? `
            <p>ФИО: ${item.user.full_name}</p>
            <p>Телефон: ${item.user.phone}</p>
          `
              : ""
          }
          <p>Мастер: ${item.master.name}</p>
          <p>Статус: ${
            isAdmin
              ? `
            <form 
              hx-patch="/change-status/${item.id}"
              hx-trigger="submit"
              hx-target="#message"
              hx-swap="outerHTML"
              class="flex gap-1">
              <select
                id="id_status"
                name="id_status"
                defaultValue
                class="transition-colors flex h-8 w-full rounded-md border border-gray-300 bg-white px-3 py-2 text-sm ring-offset-white file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-gray-500 focus-visible:outline-none hover:border-gray-400 focus-visible:border-gray-400"
              >
                ${statuses
                  .map(
                    (status, statusIndex) => `
                    <option ${
                      item.id_status === status.id && `selected="selected"`
                    } id="status-option-${index}-${statusIndex}" value=${
                      status.id
                    }>${status.name}</option>
                  `
                  )
                  .join("")}
              </select>
              <button
                type="submit"
                class="outline-none p-1 transition-colors inline-flex items-center justify-center whitespace-nowrap rounded-md text-sm font-medium bg-gray-950 text-white hover:bg-gray-800"
              >Сменить статус</button>
            </form>
            `
              : item.status.name
          }</p>
          <p>Бронирование: ${format(
            item.booking_datetime,
            "dd-MM-yyy hh:mm:ss"
          )}</p>
      </section>
    `
        )
        .join("")
    );
  } catch (error) {
    return res
      .status(400)
      .send(getToastMessage("bg-red-600", "Что-то пошло не так"));
  }
});

export default router;
